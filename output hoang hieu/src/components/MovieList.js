import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Header from './common/Header';
import Footer from './common/Footer';

import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { Grid } from '@mui/material';


const MovieList = () => {
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    axios.get('https://api.themoviedb.org/3/movie/popular', {
      params: {
        api_key: 'caa5fc3080f08d62d1fb212a5af619b6'
      }
    })
      .then(response => {
        setMovies(response.data.results);
      })
      .catch(error => {
        console.error(`Error making API request: ${error}`);
      });
  }, []);

  return (
    <>
      <Header />
      <div>
        <Grid container spacing={2}>
          {movies.map(movie => (
            <Grid item xs={4} sm={3} md={2}>
              <div key={movie.id}>
                <Link to={`/movies/${movie.id}`}>
                  <Card sx={{ minWidth: 275, minHeight: 450 }}>
                    <CardContent>
                      <Typography>
                        <img src={`https://image.tmdb.org/t/p/w220_and_h330_face/${movie.poster_path}`} alt=''></img>
                      </Typography>
                      <Typography gutterBottom variant="h5" component="div">
                        {movie.title}
                      </Typography>
                    </CardContent>
                  </Card>
                </Link>
              </div>
            </Grid>
          ))}
        </Grid>

      </div>

      <Footer />
    </>

  );
};

export default MovieList;