import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import Header from './common/Header';
import Footer from './common/Footer';
import { Grid, Typography, Card, CardContent, CardMedia, Chip, Container } from '@mui/material';



const MovieDetail = () => {
  const { movieId } = useParams();
  const [movie, setMovie] = useState(null);

  useEffect(() => {
    axios.get(`https://api.themoviedb.org/3/movie/${movieId}`, {
      params: {
        api_key: 'caa5fc3080f08d62d1fb212a5af619b6'
      }
    })
      .then(response => {
        setMovie(response.data);
      })
      .catch(error => {
        console.error(`Error making API request: ${error}`);
      });
  }, [movieId]);

  if (!movie) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <Header />
      <Container maxWidth="md">
        <Card>
          <CardMedia
            component="img"
            image={`https://image.tmdb.org/t/p/w500${movie.poster_path}`}
            alt={movie.title}
            sx={{
              height: 300, // adjust the height to your desired value
              maxWidth: 200, // adjust the width to your desired value
              objectFit: 'cover', // optional, to maintain the aspect ratio
            }}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              {movie.title}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              {movie.tagline}
            </Typography>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Typography variant="body2" color="text.secondary">
                  Release Date: {movie.release_date}
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="body2" color="text.secondary">
                  Runtime: {movie.runtime} minutes
                </Typography>
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Typography variant="body2" color="text.secondary">
                  Genres:
                  {movie.genres.map((genre) => (
                    <Chip key={genre.id} label={genre.name} variant="outlined" />
                  ))}
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="body2" color="text.secondary">
                  Production Companies:
                  {movie.production_companies.map((company) => (
                    <Chip key={company.id} label={company.name} variant="outlined" />
                  ))}
                </Typography>
              </Grid>
            </Grid>
            <Typography variant="body2" color="text.secondary">
              Overview: {movie.overview}
            </Typography>
          </CardContent>
        </Card>
      </Container>

      <Footer />
    </>

  );
};

export default MovieDetail;