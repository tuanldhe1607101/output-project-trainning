import React from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import MovieList from './components/MovieList';
import MovieDetail from './components/MovieDetail';

const router = createBrowserRouter([
  {
    path: '/',
    element: <MovieList />
  },
  {
    path: '/movies/:movieId',
    element: <MovieDetail />
  }
]);

const App = () => {
  return <RouterProvider router={router} />;
};

export default App;