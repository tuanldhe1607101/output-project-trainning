import { createContext, useState, useContext } from 'react';

const MovieContext = createContext();

export function useMovie() {
  return useContext(MovieContext);
}

export function MovieProvider({ children }) {
  const [selectedMovie, setSelectedMovie] = useState(null);

  return (
    <MovieContext.Provider value={{ selectedMovie, setSelectedMovie }}>
      {children}
    </MovieContext.Provider>
  );
}