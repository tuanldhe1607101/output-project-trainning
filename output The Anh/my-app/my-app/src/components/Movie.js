import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useMovie } from "./MovieContext";

function Movie() {
  const [movieList, setMovieList] = useState([]);
  const [movieDetail, SetMovieDetail] = useState(null);
  const navigate = useNavigate();
  const { setSelectedMovie } = useMovie();

  const getMovie = () => {
    fetch(
      "https://api.themoviedb.org/3/discover/movie?api_key=323e3fe5a8237f5319c4b400fb4bd2d9"
    )
      .then((res) => res.json())
      .then((json) => setMovieList(json.results));
  };

  useEffect(() => {
    getMovie();
    setSelectedMovie(movieDetail);
  }, []);



  return (
    <div style={{ width: "100%", backgroundColor: '#202124', color: "white" }}>
      <div>
        <div style={{
          width: '100%', fontWeight: "bolder", fontSize: '25px', height: 'auto',
          textAlign: "center", borderBottom: '1px solid black', padding: "10px 0",
          backgroundColor: '#25272d'
        }}>
          <Link to={`/`}>
            <p style={{ margin: "0", cursor: "pointer", textDecoration: "none", color: "white" }}>The Cinema </p>
          </Link>
        </div>
      </div>
      <div style={{ display: 'flex', justifyContent: "space-evenly", flexWrap: 'wrap', width: "100%" }}>
        {movieList.map((movie) => (
          <div style={{ cursor: "pointer", width: "calc(100%/5)", textAlign: "center" }}>
            <Link to={`/detailMovie?id=${movie.id}`}>
              <img
                alt={movie.original_title}
                style={{
                  width: "90%",
                  height: "300px",
                  marginLeft: "10px",
                  marginTop: "30px",
                }}
                src={`https://image.tmdb.org/t/p/w500${movie.poster_path}`}
              />
            </Link>
            <p style={{fontWeight: "bold", maxWidth: "100%", textAlign: "center"}}>{movie.original_title}</p>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Movie;
