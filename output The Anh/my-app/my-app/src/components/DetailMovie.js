import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { Link } from "react-router-dom";

function DetailMoive() {
  const searchParams = new URLSearchParams(useLocation().search);
  const [movieDetail, setMovieDetail] = useState(null);
  useEffect(() => {
    const id = searchParams.get("id");
    getDetailMovie(id);
  }, []);

  const getDetailMovie = (movieId) => {
    fetch(
      `https://api.themoviedb.org/3/movie/${movieId}?api_key=323e3fe5a8237f5319c4b400fb4bd2d9`
    )
      .then((res) => res.json())
      .then((json) => setMovieDetail(json));
  };



  return (
    <div style={{width: "100%", minHeight: "100vh", backgroundColor: '#202124', color: "white"}}>
      {movieDetail &&
        <>
          <div>
            <div style={{ width: '100%', fontWeight: "bolder", fontSize: '25px', height: 'auto',
             textAlign: "center", borderBottom: '1px solid black', padding: "10px 0",
             backgroundColor: '#25272d' }}>
              <Link to={`/`}>
                <p style={{ margin: "0", cursor: "pointer", textDecoration: "none", color: "white" }}>The Cinema </p>
              </Link>
            </div>
          </div>
          <div style={{ display: "flex", justifyContent: "space-evenly" }}>
            <div style={{ maxWidth: '30%' }}>
              <img
                alt={movieDetail.title}
                style={{
                  width: "90%",
                  height: "auto",
                  marginLeft: "10px",
                  marginTop: "30px",
                  border: "1px solid black"
                }}
                src={`https://image.tmdb.org/t/p/w500${movieDetail.poster_path}`}
              />
            </div>

            <div style={{ minWidth: "65%" }}>
              <div>
                <p style={{ fontSize: '30px', fontWeight: "bold" }}>{movieDetail.title}</p>
              </div>

              <div>
                <p style={{ fontSize: '18px' }}>Scores: {movieDetail.vote_average}/10</p>
              </div>

              <div>
                <p style={{ fontSize: '18px' }}>Release Date: {movieDetail.release_date}</p>
              </div>

              <div style={{ width: "70%" }}>
                <p style={{ fontSize: '20px' }}>{movieDetail.overview}</p>
              </div>

              {/* <div>
                <button style={{
                  padding: "30px 100px", borderRadius: "20px", backgroundColor: "#95b2f0", color: "darkblue",
                  fontSize: "20px", fontWeight: 'bold', cursor: "pointer"
                }} onClick={() => {
                  const linkOpen = movieDetail.homepage;
                  if (linkOpen) {
                    window.open(linkOpen, '_blank');
                  } else {
                    console.error('URL is not defined');
                  }
                }}>Book now</button>
              </div> */}
            </div>
          </div>
        </>
      }
    </div>
  );
}

export default DetailMoive;
