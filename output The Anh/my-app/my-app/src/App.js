import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Movie from "./components/Movie";
import DetailMoive from "./components/DetailMovie";
import { MovieProvider } from "./components/MovieContext";

function App() {
  return (
    <Router>
      <MovieProvider>
        <Routes>
          <Route path="/" element={<Movie />} />
          <Route path="/detailMovie" element={<DetailMoive />} />
        </Routes>
      </MovieProvider>
    </Router>
  );
}

export default App;
